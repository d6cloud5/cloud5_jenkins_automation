#!/bin/bash


# Install docker
sudo yum install -y docker
systemctl start docker

# make jenkins user
sudo useradd -m jenkins

# Create and set correct permissions for Jenkins mount directory
sudo mkdir -p $JENKINS_HOME
sudo chmod -R 777 $JENKINS_HOME
sudo chown -R jenkins:jenkins $JENKNIS_HOME
sudo chown jenkins:jenkins /var/run/docker.sock

# Start Jenkins
docker run \
  --privileged \
  -d \
  --rm \
  -u jenkins \
  -p 80:8080 \
  -v /opt/jenkins_home:/var/jenkins_home \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v "$HOME":/root \
  --name jenkins-dev \
  jenkinsci/blueocean

# TODO here: Write a logger.
