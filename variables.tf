

variable "region" {
  description = "The AWS region to create resources in."
  default = "us-east-1"
}

variable "availability_zone" {
  description = "The availability zone"
  default = "us-east-1b"
}

variable "jenkins_name" {
  description = "The name of the Jenkins server."
  default = "jenkins"
}

variable "amis" {
  description = "Which AMI to spawn."
  default = {
    us-east-1 = "ami-010f217b90373286c"
  }
}

variable "instance_type" {
  default = "t2.micro"
}

variable "key_name" {
  default = "devops-d6-test"
  description = "SSH key name in your AWS account for AWS instances."
}

variable "s3_bucket" {
  default = "cloud5-dev-terraform"
  description = "S3 bucket where remote state and Jenkins data will be stored."
}
